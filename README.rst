This repo tracks content of https://static.arenaoftitans.com and allows us to deploy new content to it.

It relies on `git-lfs <https://git-lfs.github.com/>`__. You need to install ``git-lfs`` to use this repo.


Usage
=====

#. Track new file types: ``git lfs track *.EXT``.
#. Add and commit as usual.
#. Get the latest LFS object for master: ``git lfs fetch master``

Update Rollbar
--------------

#. Go to https://cdnjs.com/libraries/rollbar.js
#. Download the latest version.
#. Put it in ``js``.
